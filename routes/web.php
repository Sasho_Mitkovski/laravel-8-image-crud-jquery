<?php

use App\Http\Controllers\StudentController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('add-student', [StudentController::class, 'addStudent'])->name('addStudent');
Route::post('add-student', [StudentController::class, 'storeStudent'])->name('student.store');
Route::get('all-student', [StudentController::class, 'students'])->name('students');
Route::get('edit-student/{id}', [StudentController::class, 'editStudent'])->name('student.edit');
Route::post('update-student', [StudentController::class, 'updateStudent'])->name('student.update');
Route::get('delete-student/{id}', [StudentController::class, 'deleteStudent']);

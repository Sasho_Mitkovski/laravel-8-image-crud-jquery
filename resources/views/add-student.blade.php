@extends('layouts.app')
@section('content')
<section stype="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        Додај нов студент
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('student.store') }}" enctype="multipart/form-data">
                           @if(Session::has('student_added'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('student_added') }}
                              </div>
                              @endif
                            @csrf
                        <div class="form-group">
                            <label for="name">Име</label>
                            <input type="text" name="name" class="form-control" placeholder="Име на студентот"/>
                        </div>
                        <div class="form-group">
                            <label for="File">Слика за профил</label>
                            <input type="file" name="file" class="form-control" onchange="previewFile(this)"/>
                            <img id="previewImg" alt="profile image" style="max-width:130px;margin-top:20px"/>
                        </div>
                        <button class="btn btn-primary" type="submit">Зачувај</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function previewFile(input){
    var file=$("input[type=file]").get(0).files[0];
    if(file)
    {
        var reader = new FileReader();
        reader.onload =function(){
            $('#previewImg').attr("src", reader.result)
        }
        reader.readAsDataURL(file);
    }
}
</script>
@endsection



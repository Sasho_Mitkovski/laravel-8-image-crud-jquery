@extends('layouts.app')
@section('content')
<section stype="padding-top:60px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                       Уреди профил
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('student.update') }}" enctype="multipart/form-data">
                           @if(Session::has('student_updated'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('student_updated') }}
                              </div>
                              @endif
                            @csrf
                            <input type="hidden" name="id" value="{{ $student->id }}">
                        <div class="form-group">
                            <label for="name">Име</label>
                            <input type="text" name="name" class="form-control" value="{{ $student->name }}"/>
                        </div>
                        <div class="form-group">
                            <label for="File">Слика за профил</label>
                            <input type="file" name="file" class="form-control"/>
                            <img id="previewImg" alt="profile image" src="{{ asset('images') }}/{{ $student->profileimage }}" style="max-width:130px;margin-top:20px"/>
                        </div>
                        <button class="btn btn-primary" type="submit">Зачувај</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function previewFile(input){
    var file=$("input[type=file]").get(0).files[0];
    if(file)
    {
        var reader = new FileReader();
        reader.onload =function(){
            $('#previewImg').attr("src", reader.result)
        }
        reader.readAsDataURL(file);
    }
}
</script>
@endsection



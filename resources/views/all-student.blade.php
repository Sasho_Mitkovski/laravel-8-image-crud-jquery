@extends('layouts.app')
@section('content')

    <section stype="padding-top:60px;">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="card">

                        <div class="card-header d-flex justify-content-between">
                            <h4> Сите студенти</h1><a href="{{ route('addStudent') }}"
                                    class="btn btn-success ml-right">Додај нов студент</a>
                        </div>
                        <div class="card-body">
                            @if (Session::has('student-deleted'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('student-deleted') }}
                            </div>
                        @endif
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Photo</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($students as $student)
                                        <tr>
                                            <td><small>Име</small>
                                                <h4 class=" text-primary">{{ $student->name }}</h4>
                                            </td>
                                            <td>
                                                <small>Профилна слика на {{ $student->name }}</small><br>
                                                <img src="{{ asset('images') }}/{{ $student->profileimage }}"
                                                    style="max-width:60px;">
                                            </td>
                                            <td>
                                                <a href="{{ route('student.edit', $id = $student->id) }}"
                                                    class="btn btn-info m-4">Edit</a>
                                                <a href="/delete-student/{{ $student->id }}"
                                                    class="btn btn-danger  m-4">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
